import java.util.Arrays;

public class ComputeParities{
	
	private byte[][][] data;
	private byte[][][] rowsParity;
	private byte[][][] colsParity;
	private byte[] moduleParity;
	
	//constructor
	public ComputeParities(byte[][][] data){
		this.data = data;
		this.moduleParity = new byte[data.length];
		this.rowsParity = new byte[data.length][4][7];
		this.colsParity = new byte[data.length][7][4];
	}

	public byte ComputeModuleSum(byte[][] data){
    	byte sum = 0; 
    	for(int row = 0; row < data.length; row++){ 
    		for(int col = 0; col < data[row].length; col++){ 
    			sum ^= data[row][col]; 
    		}
    	}
    	return sum; 
    }


    public void ComputeSum(){
    
    	for(int module = 0; module < this.data.length; module++){

    		this.moduleParity[module] = ComputeModuleSum(this.data[module]);

    		int x =0, b = 0; 

    		for(int i =0; i < this.data[module].length; i++){
    			for(int j =0; j < this.data[module][i].length; j++){


    				this.rowsParity[module][i][6] ^= this.data[module][i][j];

    				for(int k = j+1; k < this.data[module][i].length; k++){
    					this.colsParity[module][x][i] = (byte)(this.data[module][j][i] ^ this.data[module][k][i]);
    					x++;
    				}
    				

    				for(int l = j+1; l < this.data[module][i].length; l++){
    					this.rowsParity[module][i][b] = (byte)(this.data[module][i][j] ^ data[module][i][l]);
    					b++;
    				}
    			

    			}
    			x = 0;
    			b = 0;
    		}
    	
    		this.colsParity[module][6] = this.data[module][3];
    	}

    }
    public byte[][][] getColsParity(){
    	return this.colsParity;
    }
    
    public byte[][][] getRowsParity(){
    	return this.rowsParity;
    }
    public byte[] getModuleParity(){
    	return this.moduleParity;
    }
    
    public String toString(){
		String s = "";
		for (byte[][] b : this.colsParity){
			for (byte[] f : b){
				s += Arrays.toString(f);
				s += "\n";
			}
			s += "\n";
		}
//		for (byte[][] b : this.colsParity){
//			for (byte[] f : b){
//				s += Arrays.toString(f);
//				s += "\n";
//			}
//			s += "\n";
//		}

		return s;
	}

}