import org.apache.commons.io.*;
import java.io.File;
import java.util.Arrays;
public class Data {

	private byte[][][] array;

	public Data(String fileName) throws Exception {
		byte [] fileAsByteArray = FileUtils.readFileToByteArray(new File(fileName));
		int i=0;
		int four = 4;
		int breadth = (int) Math.ceil(fileAsByteArray.length / 16.0 );
		
		array = new byte[breadth][four][four];
		for (int j=0; j<breadth; j++){
			for (int k = 0; k < four; k++){
				for(int l=0; l < four; l++){
					if (i < fileAsByteArray.length){
						array[j][k][l] = fileAsByteArray[i++];
					}
					else {
						array[j][k][l] = 1;
						i++;
					}
				}
			}
		}
	}

	public String toString(){
		String s = "";
		for (byte[][] b : array){
			for (byte[] f : b){
				s += Arrays.toString(f);
				s += "\n";
			}
			s += "\n";
		}

		return s;
	}
	
	public byte[][][] getData(){
		return array;
	}
}
