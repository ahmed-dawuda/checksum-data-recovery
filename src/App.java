
public class App {
	public static void main(String[] args) throws Exception{
		byte[][][] d = {
				{
					{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}
				},
				{
					{17,18,19,20},{21,22,23,24},{25,26,27,28},{27,71,1,4}
				},
				{
					{4,12,8,4},{22,13,45,2},{54,76,1,34},{23,1,2,3}
				},
				{
					{7,5,9,2},{5,6,7,12},{44,12,76,34},{44,7,65,12}
				}
		};
		byte[][][] der = {
				{
					{18,2,31,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}
				},
				{
					{17,18,19,20},{21,22,23,24},{2,26,27,28},{27,71,1,4}
				},
				{
					{4,12,8,4},{22,13,45,2},{54,76,1,34},{23,1,2,3}
				},
				{
					{7,5,9,2},{5,6,7,12},{44,12,76,34},{44,7,65,12}
				}
		};
				
				
//		Data data = new Data("C:/Users/biilsroom/Documents/File.java");
//		System.out.println(data);
		ComputeParities parity = new ComputeParities(d);
		parity.ComputeSum();
//		System.out.println(parity);
		LocateError error = new LocateError(der, parity.getRowsParity(), parity.getColsParity(),parity.getModuleParity());
		error.LocateDeffectedModule();
	}
}
