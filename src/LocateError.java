import java.util.Arrays;

public class LocateError {
	private byte[][][] data;
	private byte[][][] rowsParity;
	private byte[][][] colsParity;
	private byte[] moduleParity;

	public LocateError(byte[][][] data, byte[][][] rowsParity, byte[][][] colsParity, byte[] moduleParity){
		this.data = data;
		this.rowsParity = rowsParity;
		this.colsParity = colsParity;
		this.moduleParity = moduleParity;
	}

	public void LocateDeffectedModule(){
		boolean errorExist = false;

		for(int i =0; i < data.length; i++){
			if(ComputeModuleSum(data[i]) != moduleParity[i]){
				System.out.println("module "+(i+1)+" has error");
				LocateErrorInModule(this.data[i],i);
				errorExist = true;
			}
		}
		if(!errorExist)
		  System.out.println("no error exist");
	}



	public byte ComputeModuleSum(byte[][] d){
    	byte sum = 0; 
    	for(int row = 0; row < d.length; row++){ 
    		for(int col = 0; col < d[row].length; col++){ 
    			sum ^= d[row][col]; 
    		}
    	}
    	return sum; 
    }

    protected  boolean checkSum(byte[] dataShard, byte xor){
        int sum = 0; 
        for (int a : dataShard){
            sum ^= a; 
        }
        if (sum == xor)  
            return true; 
        else
            return false;
    }
    
//copy code here
    
 	private void LocateErrorInModule(byte[][] d,int x){

		int m,n,p; //declaration of variables

		boolean noError = true; //assigning the error checker a value

		for(int row = 0;row < d.length; row++){ //looping through the rows of the data

			if(!checkSum( d[row] , this.rowsParity[x][row][6] )){ //if the value computed is not true

				noError = false; //assign noError to false to show that an error exists in the data

				for(int col = 0; col < d[0].length;col++){ //looping through the columns of the data

					switch(row){ //checking the row we are working with

						case 0: //if its row 1

						m = d[row][col] ^ d[row+1][col]; //parity of the value in the row is computed with the value on the next row

						n = d[row][col] ^ d[row+2][col]; //parity of the value in the row is computed with the value two rows below it

						p = d[row][col] ^ d[row+3][col]; //parity of the value in the row is computed with the value three rows below it

						if( m != this.colsParity[x][row][col] ) //checking if the value computed is equal to the value stored in the colsParity array

							if( n != this.colsParity[x][row+1][col] ) //checking if the value computed is equal to the value stored in the colsParity array

								if( p != this.colsParity[x][row+2][col]) { //checking if the value computed is equal to the value stored in the colsParity array

									boolean isNotCorrupt = rowCheck(d,row,col,x); //checking if there is a corruption in the data
									if(!isNotCorrupt){ 
										d[row][col] = 0; //data that is corrupt is set to zero 

									System.out.println("\tcell ("+row+" , "+col+" )"); //displaying the cell that has an error

									}
								}
							
								else continue; //moves to the next value if they are the same

							else continue; //moves to the next value if they are the same

						else continue; //moves to the next value if they are the same

						break;

						case 1: //if its row 2

						m = d[row][col] ^ d[row-1][col]; //parity of the value in the row is computed with the value on the row before it

						n = d[row][col] ^ d[row+1][col]; //parity of the value in the row is computed with the value on the row below it

						p = d[row][col] ^ d[row+2][col]; //parity of the value in the row is computed with the value two rows below it

						if( m != this.colsParity[x][row-1][col]) //checking if the value computed is equal to the value stored in the colsParity array

							if(n != this.colsParity[x][row+2][col]) //checking if the value computed is equal to the value stored in the colsParity array

								if( p != this.colsParity[x][row+3][col]){ //checking if the value computed is equal to the value stored in the colsParity array

									boolean isNotCorrupt = rowCheck(d,row,col,x); //checking if there is a corruption in the data
									if(!isNotCorrupt){
										d[row][col] = 0; //data that is corrupt is turned to zero

									System.out.println("\tcell ("+row+" , "+col+" )"); //displaying the cell with an error 

									}
								}

								else continue; //moves to the next value if they are the same

							else continue; //moves to the next value if they are the same

						else continue; //moves to the next value if they are the same

						break;

						case 2: //if it's row 3 

						m = d[row][col] ^ d[row-2][col]; //parity of the value in the row is computed with the value two rows above it

						n = d[row][col] ^ d[row-1][col]; //parity of the value in the row is computed with the value one row above it

						p = d[row][col] ^ d[row+1][col]; //parity of the value in the row is computed with the value one row below it

						if(m != this.colsParity[x][row-1][col]) //checking if the value computed is equal to the value stored in the colsParity array

							if( n != this.colsParity[x][row+1][col]) //checking if the value computed is equal to the value stored in the colsParity array

								if( p != this.colsParity[x][row+3][col]){ //checking if the value computed is equal to the value stored in the colsParity array

									boolean isNotCorrupt = rowCheck(d,row,col,x); //checking if there is a corruption in the data
									if(!isNotCorrupt){
										d[row][col] = 0; //data that is corrupt is turned to zero

									System.out.println("\tcell ("+row+" , "+col+" )"); //displaying the cell with an error
									
									}
								}

								else continue; //moves to the next value if they are the same

							else continue; //moves to the next value if they are the same

						else continue; //moves to the next value if they are the same

						break;

						case 3: //if it's row 4

						m = d[row][col] ^ d[row-3][col]; //parity of the value in the row is computed with the value three rows above it

						n = d[row][col] ^ d[row-2][col]; //parity of the value in the row is computed with the value two rows above it

						p = d[row][col] ^ d[row-1][col]; //parity of the value in the row is computed with the value one row above it

						if(m != this.colsParity[x][row-2][col]) //checking if the value computed is equal to the value stored in the colsParity array

							if(n != this.colsParity[x][row+1][col]) //checking if the value computed is equal to the value stored in the colsParity array

								if(p != this.colsParity[x][row+2][col]){ //checking if the value computed is equal to the value stored in the colsParity array

									boolean isNotCorrupt = rowCheck(d,row,col,x); //checking if there is a corruption in the data
									if(!isNotCorrupt){
										d[row][col] = 0; //data that is corrupt is turned zero

									System.out.println("\tcell ("+row+" , "+col+" )");

									}
								}
								else continue; //moves to the next value if they are the same

							else continue; //moves to the next value if they are the same

						else continue; //moves to the next value if they are the same

						break;

						default:

					}
				}
			}

		}

		for(int i = d.length-1; i >= 0; i--){
			for(int j = 0; j < d[i].length; j++){
				if(d[i][j] == 0){
					d[i][j]  = this.colsParity[x][6][j];
				}
			}
			break;
		}


		while(countErrors(d) > 0){ //checking if the number of errors in the module are more than zero

			ResolveCorruption(d, x); //the ResolveCorruption method is called on the module and its coordinates

		}
    }

    protected  boolean rowCheck(byte[][] d, int row,int col,int x){

		int start=0,end=0; //initialize the start and end variables to zero 

		switch(col){ //check the col value that is passed when the method is called

			case 0: start = 0;end = 2; break; //if the value is zero, the parity values of the entry in that location start at index zero and end on index two in the rowParity array

			case 1: start = 3;end = 4; break; //if the value is one, the parity values of the entry in that location start at index three and end on index four in the rowParity array

			case 2: start = 5;end = 5; break; //if the value is two, the parity values of the entry in that location start at index five and end on index five in the rowParity array

			default:
		}

		int m ; //declaration of the variable

		for(int k=col+1;k<d[row].length-1;k++){//looping through the row of the module

			m = d[row][col] ^ d[row][k]; //assign the parity of the data in that entry with the value below it. that is, in the same column on the next row

			if(m == this.rowsParity[x][row][start]) return true; //if the value computed is the same as the value store, return true

			start += (start < end)?1:0; //the start value is increased by one if the start value is lesser than the end value, else it is maintained

		}

		return false; //return false if they are not the same
	}

	protected int countErrors(byte[][] d){

		int count = 0; //initialize count to zero

		for(int row = 0;row<d.length;row++){ //looping through the row in the module

			for(int col = 0;col<d[row].length;col++){ //looping through the column in the row

				if(d[row][col] == 0) count++; //if the value is zero, count is increased by one

			}
		}

		return count; //return count, the number of errors in the data
	}

	protected void resolve(byte[][] d, int row,int col,int x){

		int start=0,end=0;

		switch(col){ //checking the column we are working with

			case 0: start = 0;end = 2; break; //the start and end values of the column is computed 

			case 1: start = 3;end = 4; break; //the start and end values of the column is computed

			case 2: start = 5;end = 5; break; //the start and end values of the column is computed

			default:

		}

		for(int i = col+1;i<d[row].length;i++){ //looping through the rows of the data

			if(col < end && d[row][i] != 0){ //checks if the col value is less than the end val and the value in the data in the current loop is not zero

				d[row][col] = (byte)( d[row][i] ^ this.rowsParity[x][row][start]); //the value of the data is computed with its value stored in the rowsParity array and the data in the current location in the iteration

				break;

			}

			start += (start < end)? 1:0; //the value of start is computed
		}

	}

	public void ResolveCorruption(byte[][] d,int x){

		for(int row = 0;row < d.length; row++){ //looping through the data 

			for(int col = 0; col < d[row].length;col++){ //looping through the row of the data

				if( d[row][col] == 0 ){ //checks if the value in the position we are on in the iteration is zero

					resolve(d,row,col, x); //the resolve method is called
				}
			}
		}

		for(int row = 0;row<d.length;row++){ //looping through the data

			if(d[row][d[row].length-1] == 0){ //checking the value in the last column of the data

				switch(row){ //checking the row

					case 0: //if it's row 1

					d[row][3] = (byte)((d[row][2] != 0)?d[row][2]^this.rowsParity[x][row][5]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the rowsParity array and the value in the column before it else it is maintained

					d[row][3] = (byte)((d[row+1][3] != 0)?d[row+1][3]^this.colsParity[x][row][3]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the colsParity array and the value in the row below it else it is maintained

					break;

					case 1: //if it's row 2

					d[row][3] = (byte)((d[row][2] != 0)?d[row][2]^this.rowsParity[x][row][5]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the rowsParity array and the value in the column before it

					d[row][3] = (byte)((d[row-1][3] != 0)?d[row-1][3]^this.colsParity[x][row-1][3]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the colsParity array and the value in the row above it else it is maintained

					d[row][3] = (byte)((d[row+1][3] != 0)?d[row+1][3]^this.colsParity[x][3][3]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the colsParity array and the value in the row below it else it is maintained

					break;

					case 2: //if it's row 3

					d[row][3] = (byte)((d[row][2] != 0)?d[row][2]^this.rowsParity[x][row][5]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the rowsParity array and the value in the column before it

					d[row][3] = (byte)((d[row-1][3] != 0)?d[row-1][3]^this.colsParity[x][3][3]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the colsParity array and the value in the row above it else it is maintained

					d[row][3] = (byte)((d[row+1][3] != 0)?d[row+1][3]^this.colsParity[x][5][3]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the colsParity array and the value in the row below it else it is maintained

					break;

					case 3: //if it's row 4 

					d[row][3] = (byte)((d[row][2] != 0)?d[row][2]^this.rowsParity[x][row][5]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the rowsParity array and the value in the column before it

					d[row][3] = (byte)((d[row-1][3] != 0)?d[row-1][3]^this.colsParity[x][5][3]:d[row][3]); //the data in the last column is checked and if it is zero, the value is computed with its corresponding value in the colsParity array and the value in the row above it else it is maintained

					break;

					default:

				}
			}
		}
		display(d);
	}
	
	public void display(byte[][] d){
		String s = "";
		for (byte [] b : d)
			s += Arrays.toString(b) + "\n";
		System.out.println(s);
	}
}
